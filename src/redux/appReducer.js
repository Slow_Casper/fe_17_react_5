import { combineReducers } from "redux"
import storeReducer from "./store/reducer";
import favouriteReducer from "./favourite/reducer";
import cartReducer from "./cart/reducer";
import modalReducer from "./modal/reducer";


const appReducer = combineReducers({
    store: storeReducer,
    favourite: favouriteReducer,
    cart: cartReducer,
    modal: modalReducer
});

export default appReducer