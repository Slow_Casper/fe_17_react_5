import { useDispatch, useSelector } from "react-redux";
import styles from "./Modal.module.scss";
import { setModalClose, removeItem } from "../../redux/modal/actionCreators";
import { getCart } from "../../redux/cart/actionCreators";


const Modal = ({ text }) => {
    
const { isRemoveModalOpen } = useSelector(state => state.modal)
const dispatch = useDispatch()


const close = () => {
    dispatch(setModalClose())
}

const removeFromCart = () => {
    dispatch(removeItem())
    dispatch(getCart())
}

    return (
        <div className={styles.background} onClick={close}>
            <div className={styles.modal}>
                <p className={styles.text}>{text}</p>
                {!isRemoveModalOpen ? (
                <button onClick={close}>OK</button>
                ) : (
                    <div className={styles.removeButtons}>
                        <button className={styles.remove} onClick={removeFromCart}>Remove</button>
                        <button onClick={close}>Cancel</button>
                    </div>
                )}
            </div>
        </div>
    );
};

export default Modal;
