import React from 'react'
import PropTypes from 'prop-types'

const Button = ({ backgroundColor, classes, text, onClick }) => {
  return (
    <button 
        style={{
            backgroundImage: backgroundColor
        }}
        className={classes} 
        type='button'
        onClick={onClick}
    >
        {text}
    </button>
  )
}

Button.propTypes = {
    backgroundColor: PropTypes.string,
    classes: PropTypes.string,
    text: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.object
    ]),
    onClick: PropTypes.func
}

Button.defaultProps = {
    backgroundColor: "red",
    classes: "",
    text: "No text there",
    onClick: () => {}
}

export default Button