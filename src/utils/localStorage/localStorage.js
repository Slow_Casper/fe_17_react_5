export const saveToLocalStorage = (key, state) => {
    window.localStorage.setItem(key, JSON.stringify(state))
}

export const getFromLocalStorage = (key) => {
    const lsValue = window.localStorage.getItem(key);

    if (lsValue) {
        return JSON.parse(lsValue);
    } 

    return null;
}